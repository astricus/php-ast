<?php
header('Content-Type: text/html; charset=utf-8');
define("APP_HOME", __DIR__);
define("DS", DIRECTORY_SEPARATOR);
define("LOG_FILE", "app.log");
//$ip_ranges = [
//    "127.0.0.1/24",
//    "192.168.0.1/24"
//];
//$CHECK_IP_ADDR_IN_RANGE = true;// set false for aborting ip address checking in range
error_reporting(E_ALL);
ini_set("display_errors", 1);
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_regex_encoding('UTF-8');
mb_internal_encoding("UTF-8");

require_once "app_config.php";

function loadClass($class) {
    $path = APP_HOME . DS . 'lib' . DS;
    require_once $path . $class .'.php';
}
spl_autoload_register('loadClass');

$APP = new App($app_config);
$admin = new Admin("admin");
$admin->checkAuth($app_config['token_file']);

$app_mode_array = [
    "show_image",
    "default",
    "image", // ajax get image
    "clear",
    "info",// debug
];
$APP_MODE = null;

$request_uri = $APP->getAppUri();
if($request_uri == null){
    require_once "web/index.php";
}
else if($request_uri == "file_com"){
    $action = isset($_REQUEST['action']) ?  $_REQUEST['action'] :  null;
    $path = isset($_REQUEST['path']) ?  $_REQUEST['path'] :  null;
    if($path==null){
        $path = APP_HOME;
    }
    $file_man = new FileMan();
    $path_list = $file_man->getDirList($path);
    $APP->returnJson(["files" => $path_list, "path" => $path], "success");
    exit;
}
else if($request_uri == "server_usage"){
    $server_app  = new Server();
    $server = [];
    $server['datetime'] =  $server_app->serverDateTime();
    $server['cpu_usage'] =  $server_app->server_cpu_usage();
    $server['memory_usage'] = $server_app->server_memory_usage();
    $server['df'] = $server_app->disk_free_space();
    $server['info'] = $server_app->server_info();
    $server['ip'] = $server_app->ip;
    $server['os'] = $server_app->os;
    $APP->returnJson(["server" => $server], "success");
    exit;
}
else if($request_uri == "console"){
    $command = isset($_REQUEST['c']) ?  $_REQUEST['c'] :  null;
    // security command
    $console_command = new Console($command);
    $console_command->run();
    $APP->returnJson(["result" => $console_command->output], "success");
    exit;
}
else if($request_uri == "file_content"){
    $file = isset($_REQUEST['f']) ?  $_REQUEST['f'] :  null;
    $action = isset($_REQUEST['a']) ?  $_REQUEST['a'] :  null;
    $file_man = new FileMan();
    $result = $file_man->getFileContent($file);
    if(!$result){
        $error = "Файл не существует, либо является директорией, либо недоступен для чтения";
        $APP->returnJson(["file_content" => null, "error" => $error], "error");
    }
    $APP->returnJson(["file_content" => $result, "error" => null], "success");
    exit;
}
else if($request_uri == "file_delete"){
    $file = isset($_REQUEST['f']) ?  $_REQUEST['f'] :  null;
    $file_man = new FileMan();
    $result = $file_man->deleteFile($file);
    if($result){
        $APP->returnJson(["result" => "файл удален", "error" => null], "success");
    } else {
        $error = "Файл не был удален (либо не существует директорией, либо недоступен для чтения)";
        $APP->returnJson(["file_content" => null, "error" => $error], "error");
    }
    exit;
}