<?php
if (php_sapi_name() !== "cli") {
    die("installation can be launched in cli mode only!");
}
require_once "config.php";
require_once "lib/functions.php";

//$kill_running_copies = exec("ps aux | grep $app_name | head -1 | cut -d ' ' -f 2 | xargs kill");
exec('ps aux | grep ' . $app_name . ' | awk \'{print $2}\' | xargs kill');

$pass = isset($argv[1]) ? $argv[1] : null;
$port = isset($argv[2]) ? $argv[2] : null;
$dir = isset($argv[3]) ? $argv[3] : null;

if($pass==null OR mb_strlen($pass)<8){
    $pass = generateAppPass();
}
if($port == null OR $port<1000 OR $port>65000){
    $port = $default_port;
}
echo "saving your pass: " . $pass;

$pass_hash = stringHash($pass_salt, $pass);

//create file pass
$abs_pass_file = $path . DS . $pass_file;
file_put_contents($abs_pass_file, $pass_hash);
chmod($abs_pass_file, 0777);
$check_pass = file_get_contents($abs_pass_file);
if (!file_exists($abs_pass_file)) {
    die("Application can't create required key file 'pass', application is stopped...");
}

//create file log
$log_file_abs = $path. "/" . $log_file;
file_put_contents($log_file_abs, null);
chmod($log_file_abs, 0777);
if (!file_exists($log_file_abs)) {
    die("Application can't create required file $log_file_abs, application is stopped...");
}

$web_server_init_com ="php -S 0.0.0.0:$port -t $web_path";

$server_init_result = exec("$web_server_init_com &");
if(substr_count($server_init_result,  "Server started")==0){
    die("Error! Application can't start built-in web server on port $port...");
}



$type = "PHPSurfer app is running...";
$message = "PHPSurfer starts successfully";

addToLog($message, currentDatetime(), $type, $log_file);
echo $message;
exit;