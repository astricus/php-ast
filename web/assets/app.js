let transfer_data = null;
let interval = 10000;
let cur_path;

let ext_arr_image = ['jpeg', 'jpg', 'gif', 'png'];
let ext_arr_text = ['txt', 'md', 'php', 'html', 'css', 'js', 'log'];
let ext_arr_exec = [];

let new_filename_input = $('#new_filename_input');
let new_filename_save_button = $('#new_filename_save_button');
let new_filename_form = $('#new_filename_form');

$(document).ready(function () {
    function getServerUsage(){
        $.ajax({
            url: "/server_usage",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
                console.log("sending request");
            },
            success: function (data) {
                if (data.status !== "error") {
                    console.log(data.data);
                        $("#datetime__value").text(data.data.server.datetime);
                        $("#cpu_usage__value").text(data.data.server.cpu_usage);
                        $("#memory_usage__value").text(data.data.server.memory_usage);
                        $("#df__value").text(data.data.server.df);
                        $("#info__value").text(data.data.server.info);
                    }
                }
        });
    }
    getServerUsage();
    intervalName = setInterval(getServerUsage, interval);

    //console listener
    function consoleListener(command){
        let command_send = "c=" + command;

        addUsedCommand(command);

        $.ajax({
            url: "/console",
            type: "post",
            dataType: 'json',
            data: command_send,
            beforeSend: function () {
                console.log("sending console command");
            },
            success: function (data) {
                //console.log(data.data.result);
                if (data.status !== "error") {
                    $("#command_result").append("Команда: "+ command + " " + "<br>");
                    let output = data.data.result;
                    output = output.replace(/(?:\r\n|\r|\n)/g, '<br>');
                    $("#command_result").append(output);
                }
            }
        });
    }

    //console listener event
    $(document).on("click", "#console_command", function (e) {
        let command = $("#command").val().trim();
        if(command=="clear"){
            clearConsole();
            return;
        }
        if(command.length>=2) {
            consoleListener(command);
        }
        e.preventDefault();
        return true;
    });

    //console listener
    function getFileList(path){
        let file_path = "path=" + path;
        $.ajax({
            url: "/file_com",
            type: "post",
            dataType: 'json',
            data: file_path,
            beforeSend: function () {
                console.log("sending file path command");
            },
            success: function (data) {
                console.log(data);
                if (data.status !== "error") {
                    console.log(data.data.files);
                    redraw_file_list(data.data.files, data.data.path);
                    cur_path = data.data.path;
                    $(".current_path").text(data.data.path);
                }
            }
        });
    }
    getFileList("");
    $(document).on("click", "#get_file_list", function (e) {
        let path = "";
        getFileList(path);
        e.preventDefault();
        //return true;
    });
    $(document).on("click", ".file_name", function (e) {
        let file_type = $(this).attr('data-type');
        let file_path = $(this).attr('data-path');
        let file_name = $(this).attr('data-name');
        if(file_type==="dir"){
            let read_dir = file_path + "/" + file_name;
            if(file_name===".."){

                let pathSlashPos = file_path.lastIndexOf("/");
                if(pathSlashPos===0){
                    file_path = "/";
                } else {
                    file_path = file_path.substring(0, pathSlashPos);
                }
                read_dir = file_path;
            } else {
                if(file_path==="/"){
                    read_dir = "/" + file_name;
                } else {
                    read_dir = file_path + "/" + file_name;
                }
            }

            console.log(read_dir);
            getFileList(read_dir);
        }
    });


    let file_container = $("#file_container");
    let used_commands = $("#used_commands");

    let redraw_file_list = function(files, path){
        file_container.html('');
        for(let i=0; i<files.length; i++){
            let file_name = files[i].name;
            let file_icon, file_type;
            if(files[i].type=="dir"){
                file_icon = "&#128193;";
                file_type = "dir"
            } else {
                file_icon = "";
                file_type = "file"
            }
            let file_size = files[i].size;
            if(file_size==null) file_size = "";
            let file_item = "<div class='file-block' data-id='" + i + "'><span class='file_name' data-type='" + file_type + "' data-path='" + path + "' data-name='" + file_name + "'><span class='icon'>" + file_icon + "</span> " +
                file_name + "</span><span class='file_size'> " + file_size + "</span>" +
                "<span class='file_preview' data-abs='" + path + "/" + file_name + "'>&#128065;</span>" +
                "<span class='file_delete' data-abs='" + path + "/" + file_name + "' data-id='" + i + "'><i class='fas fa-trash-alt'></i></span>" +

                "</div>";
            file_container.append(file_item);
        }
    };

    let clearConsole = function(){
        $("#command_result").html('');
    };

    let addUsedCommand = function(command){
        let new_com = "<div class='used_command'>" + command + "</div>";
        used_commands.append(new_com);
    };

    $(document).on("click", ".used_command", function (e) {
        let comm = $(this).text();
        $("#command").val(comm);
        return true;
    });

    $(document).on("click", ".file_preview", function (e) {
        let file_name = $(this).attr('data-abs');
        filePreview(file_name);
        return true;
    });
    let filePreview = function(file_name){
        let type;
        if (isFileImage(file_name)) {
            // found element image
            type = "image";
        }else if(isFileText(file_name)){
            type = "text";
        }
        loadPreviewFile(file_name, type);
    };
    let isFileImage = function (file_name) {
        return ext_arr_image.includes(fileExt(file_name));
    };
    let isFileText = function (file_name) {
        return ext_arr_text.includes(fileExt(file_name));
    };
    let fileExt = function (file_name) {
        return file_name.split('.').pop();
    }

    $(document).on("click", ".file_delete", function (e) {
        let file_name = $(this).attr('data-abs');
        let file_id = $(this).attr('data-id');
        fileDelete(file_name, file_id);
        return true;
    });
    let fileDelete = function(file, file_id){
        let abs_file = "f=" + file;
        $.ajax({
            url: "/file_delete",
            type: "post",
            dataType: 'json',
            data: abs_file,
            beforeSend: function () {
                console.log("delete file" + file);
            },
            success: function (data) {
                console.log(data.data.file_content);
                if (data.status !== "error") {
                    $(".file-block['data-id='" + file_id + "'").remove();
                    getFileList(cur_path);
                }
            }
        });
    }
    let preview_content = $("#preview_content");
    let preview_content_block = $("#preview_content_block");

    let loadPreviewFile = function(file_name, type){
        if(type==="image"){
            let image = "<img src='" + file_name + "' alt='preview " + file_name + "'/>";
            preview_content.html(image);
            preview_content.show();
        } else if(type==="text"){
            fileGetTextContent(file_name, preview_content);
            preview_content.show();
        } else {
            console.log("undefined file type detected");
        }

    }

    let fileGetTextContent = function (file_name, container) {
        let file = "f=" + file_name;
        $.ajax({
            url: "/file_content",
            type: "post",
            dataType: 'json',
            data: file,
            beforeSend: function () {
                console.log("get file content");
            },
            success: function (data) {
                console.log(data.data.file_content);
                if (data.status !== "error") {
                    preview_content_block.html(data.data.file_content);
                }
            }
        });
    }

    $(document).on("click", ".make_text_file", function (e) {
        $('#preview_content_block').focus();
        $("#new_filename_form").show();
        return true;
    });


});