<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHPSurfer</title>
    <script src="web/assets/jquery.min.js"></script>
    <script src="web/assets/app.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c44644874e.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="web/assets/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function(){
            $(document).tooltip();
        });
    </script>
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-10">
            <div class="main_logo">PHPS - Панель управления сервером на php</div>
        </div>
        <div class="col-2 header_right">
            <a href="<?php echo $_SERVER['HTTP_HOST'] . "/?action=logout"; ?>">Выход</a>
        </div>
    </div>

    <div class="row">
        <div class="block col">
            <h3>Информация о сервере</h3>

            <div class="server_params">
                <div class="server_param">Сервер</div>
                <div class="server_param_value" id="info__value">&#128337;</div>
            </div>

            <div class="server_params">
                <div class="server_param">Datetime</div>
                <div class="server_param_value" id="datetime__value">&#128337;</div>
            </div>
            <div class="server_params">
                <div class="server_param">CPU</div>
                <div class="server_param_value"
                     id="cpu_usage__value">&#128337;
                </div>
            </div>
            <div class="server_params">
                <div class="server_param">Memory</div>
                <div class="server_param_value"
                     id="memory_usage__value">&#128337;
                </div>
            </div>
            <div class="server_params">
                <div class="server_param">Disk Space</div>
                <div class="server_param_value" id="df__value">&#128337;</div>
            </div>

            <a href="/phpinfo" target="_blank" class="button_link php_info">PHPINFO</a>


        </div>
        <div class="block col">
            <h3>Консоль управления</h3>
            <form name="remote php shell" action="/shell" method="post">
                <textarea name="command" id="command" placeholder="Команда"></textarea>
                <div id="console_command"> Выполнить</div>
                <div class="used_commands" id="used_commands"></div>

                <h5>Результат команды</h5>
                <div id="command_result"></div>
            </form>
        </div>
        <div class="block col">
            <h3>Файловый менеджер</h3>
            директория: <span class="current_path"></span>
            <br>
            <div class="dir-block" id="file_container"></div>
            <div class="file_control_panel">
                <div class="make_text_file blue_text click_button" title="Создать текстовый файл"><i class="fas fa-file-alt"></i></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="preview_content" class="col block">
            <h3>Просмотр файла "<span class="preview_content_title"></span>"</h3>
            <textarea id="preview_content_block"></textarea>

            <div id="new_filename_form" class="">
                <input id="new_filename_input" class="" value="" name="new_filename_input" placeholder="Название нового файла"/>
                <div id="new_filename_save_button" class="button_link">создать файл</div>
            </div>

        </div>
    </div>

    <div class="row footer">
        <div class="col-3">
            <span class="info">версия 1.01</span>
        </div>
        <div class="col-3">
            <span class="info"><a href="https://bitbucket.org/astricus">автор</a></span>
        </div>
        <div class="col-3">
            <span class="info"><a href="https://bitbucket.org/astricus/phps">обновить панель</a></span>
        </div>
    </div>

</div>

</body>
</html>