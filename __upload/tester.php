<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once "config.php";
require_once "functions.php";

if ($CHECK_IP_ADDR_IN_RANGE) {
    $ip = $_SERVER['REMOTE_ADDR'];
    ip_range_validator($ip, $ip_ranges,  $log_file);
}
$get_pass = @file_get_contents($pass_file);
?>

    <h1>Send File Test Form</h1>
    <p>You can use this form for testing (sending image file by method "Set")</p>

<form method="post" action="/show_image?share_key=<?=$get_pass?>" enctype="multipart/form-data">
    <input type="file" name="file">
    <input type="submit" value="Send File">
</form>

    <hr>
    <h1>Deleting Image Files </h1>
    <a href="/clear?share_key=<?=$get_pass?>">Delete image files</a>
<hr>
<?php
    if(!file_exists($default_image)){
    echo "<h3 style='color: red'>Error! Default image file is not found, please, create default file image at $default_image</h3>";

    }/*?>


    <h1>Замена файла изображения по умолчанию </h1>
    <p>Через эту форму можно загрузить файл изображения, показываемого по умолчанию</p>

    <form method="post" action="/default?p=<?=$get_pass?>" enctype="multipart/form-data">
        <input type="file" name="file">
        <input type="submit" value="Отправить">
    </form>
<?php*/?>