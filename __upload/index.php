<?php
header('Content-Type: text/html; charset=utf-8');
require_once "config.php";
require_once "functions.php";
require_once "controllers/image_processor.php";

//check ip address mask
if ($CHECK_IP_ADDR_IN_RANGE) {
    $ip = $_SERVER['REMOTE_ADDR'];
    ip_range_validator($ip, $ip_ranges,  $log_file);
}

$pass_file_data = @file_get_contents($pass_file);

$app_mode_array = [
    "show_image",
    "default",
    "image", // ajax get image
    "clear",
    "info",// debug
];

$APP_MODE = null;
$request_uri = str_replace("/", "", $_SERVER['REQUEST_URI']);
if (substr_count($request_uri, "?") > 0) {
    $R_uri = explode("?", $request_uri);
    $request_uri = $R_uri[0];
}

//  application setup method

if (in_array($request_uri, $app_mode_array)) {
    $APP_MODE = "ARM";
} else {
    $APP_MODE = "DIP";
}
if ($APP_MODE == "DIP") {
    header("location: http://" . $app_name . "/assets/index.php");
    exit;
}
//info
if ($request_uri == "info") {
    pr($_SERVER);
} //Set Image Method
else if ($request_uri == "show_image") {
    // auth by key
    auth($pass_file,  $log_file);

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        // The request is using the POST method
        die("waiting post http request method with attached image");
    }
    $img_prc = new ImageProcessor;
    $config = [
        'img_path' => $img_path,
        'def_ext' => $def_ext,
        'img_file_name' => $img_file_name,
        'max_file_size' => $max_file_size,
        'log_file' => $log_file,
        "default" => $default_image
    ];
    $md5_hash = isset($_POST['md5']) ? $_POST['md5'] : null;
    $img_prc->setConfig($config);
    $img_prc->setData($_FILES, $md5_hash);
    returnSuccess("OK");
}
//save default image
else if ($request_uri == "default") {
    // auth by key
    auth($pass_file, $log_file);

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        // The request is using the POST method
        die("waiting post http request method with attached image");
    }
    $img_prc = new ImageProcessor;
    $config = [
        'img_path' => $img_path,
        'def_ext' => $def_ext,
        'img_file_name' => $img_file_name,
        'max_file_size' => $max_file_size,
        'log_file' => $log_file,
        "default" => $default_image
    ];
    $img_prc->setConfig($config);
    $img_prc->setDefault($_FILES);
    returnSuccess("OK");
}
//OFF Image Method
else if ($request_uri == "clear") {
    $dir_handle = APP_HOME . DS . "images";
    $handle = opendir($dir_handle);
    while ($file = readdir($handle)) {
        if ($file != "." && $file != "..") {
            if (substr($file, 0, 4) == "img_") {
                unlink($dir_handle . DS . $file);
            }
        }
    }
    file_put_contents(APP_HOME . DS . $img_file_name, null);
    $type = "Method OFF";
    $message = "images was deleted";
    addToLog($message, currentDatetime(), $type, $log_file);
    returnSuccess("OK");
} // GET CURRENT IMAGE NAME BY AJAX
else if ($request_uri == "image") {
    $filename = file_get_contents($abs_filename);
    $img_file = APP_HOME . DS . "images" . DS . $filename;
    if (!file_exists($img_file) or $filename == "") {
        $data = ["status" => "error", "error" => "file not found"];
        echo json_encode($data);
        exit;
    }
    //update status call time
    file_put_contents(APP_HOME . DS . "status", time());
    $relative_name = $_SERVER['REQUEST_SCHEME'] . "://" . $app_name . "/images/" . $filename;
    $data = ["status" => "success", "image" => $relative_name];
    echo json_encode($data);
    exit;
}