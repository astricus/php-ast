<?php
require_once "../config.php";
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>DocViewer</title>
    <script src="jquery.min.js"></script>
    <script>
        let default_image = "<?php echo $default_image_relative; ?>";
    </script>
    <script src="app.js"></script>

</head>
<body>
<img id="image" src="<?php echo $default_image_relative; ?>" alt="document image" style="width: 100%; height: 100%">

</body>
</html>