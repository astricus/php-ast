let transfer_data = null;
let interval = 1500;
let intervalName;
let counter_max = 200;//для боевого приложения поставить 5 минут, то есть, 1500*200; 200 циклов
let counter=0;
let last_image = "";
$(document).ready(function () {
    function getImage(){
        $.ajax({
            url: "/image",
            type: "get",
            dataType: 'json',
            data: transfer_data,
            beforeSend: function () {
                console.log("sending");
            },
            success: function (data) {
                if (data.status !== "error") {
                    $("#image").attr("src", data.image);
                    if(last_image!=data.image)
                    {
                        counter = 0;
                        last_image = data.image;
                    }
                    counter++;
                    if(counter>=counter_max){
                        $("#image").attr("src",  default_image);
                        $.ajax({
                            url: "/off",
                            type: "get",
                            dataType: 'html',
                            data: null,
                            beforeSend: function () {
                                console.log("set method off");
                            },
                            success: function (data) {}
                        });
                        counter = 0;
                    }

                } else {
                    $("#image").attr("src",  default_image);
                    console.log(data);
                    counter = 0;
                }

            }
        });
    }

    intervalName = setInterval(getImage, interval);
});

let reload_interval = 15;
reload_interval = reload_interval*60*1000;
setTimeout(function (){window.location.reload();}, reload_interval);
