<?php
define("APP_HOME", __DIR__);
define("DS", DIRECTORY_SEPARATOR);
$ip_ranges = [
    "127.0.0.1/24",
    "192.168.0.1/24"
];
$CHECK_IP_ADDR_IN_RANGE = true;// set false for aborting ip address checking in range

$img_path = "images";
$def_ext = "jpg";
$img_file_name = "filename";
$abs_filename = APP_HOME . DS . $img_file_name;
$pass_file = APP_HOME . DS . "pass";
$status_file = APP_HOME . DS . "status";
$log_file = APP_HOME . DS . "app.log";
$max_file_size = 50*1024*1024;//Mb
$image_dir =  APP_HOME . DS . $img_path;
$file_default = "default.jpg";
$default_image = $image_dir . DS . $file_default;
$app_name = "docviewer.local";
$default_image_relative = "http://" . $app_name . "/" . $img_path . "/" . $file_default;