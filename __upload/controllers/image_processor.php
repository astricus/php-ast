<?php
require_once "config.php";
require_once "functions.php";

Class ImageProcessor {

    public $img_file_name;
    public $img_dir;
    public $def_ext;
    public $max_file_size;
    public $log_file;
    public $default;

    public $valid_format_type = ["JPEG", "GIF", "PNG"];

    const file_is_too_big_error = "file_is_too_big";
    const file_md5_hash_mismatch_error = "md5_mismatch";
    const wrong_format_error = "wrong_format";
    const file_is_empty_error = "file_is_empty";
    const image_not_attached_error = "image_not_attached";
    const filename_is_is_not_exists = "filename saver file is not exists, please create file with name 'filename' is home project dir";

    /**
     * @param $data
     */
    public function setDefault($data){
        if(!key_exists("file", $data) OR count($data) == 0 OR !is_array($data)){
            returnError(self::image_not_attached_error);
        }
        $file = $data['file'];
        $tmp_file = $file['tmp_name'];
        if(!$this->checkFile($tmp_file)){
            returnError(self::file_is_too_big_error);
        }
        move_uploaded_file($tmp_file, $this->default);
        chmod($this->default, 0777);

        $type = "set_default_image";
        $message = "default image file was changed";
        addToLog($message, currentDatetime(), $type, $this->log_file);
        return;
    }

    /**
     * @param $tmp_file
     * @return bool
     */
    private function checkFileFormat($tmp_file){
        $file_match = false;
        $format_string = exec("file $tmp_file");
        foreach ($this->valid_format_type as $type){
            if(substr_count($format_string, $type)>0){
                $file_match = true;
                break;
            }
        }
        if($file_match){
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @param $md5_hash
     */
    public function setData($data, $md5_hash){
        $file = $data['file'];
        $tmp_file = $file['tmp_name'];
        if(!$this->checkFileFormat($tmp_file)){
            returnError(self::wrong_format_error);
        }
        if(!$this->checkFileIsEmpty($tmp_file)){
            returnError(self::file_is_empty_error);
        }
        if(!$this->checkFile($tmp_file)){
            returnError(self::file_is_too_big_error);
        }
        if($md5_hash != null){
            if(!$this->checkHashSum($file, $md5_hash)){
                returnError(self::file_md5_hash_mismatch_error);
            }
        }
        $this->saveNewImage($tmp_file, $this->def_ext);
    }

    /**
     * @param $file
     * @param $md5_hash
     * @return bool
     */
    private function checkHashSum($file, $md5_hash){
        if(md5_file($file) == $md5_hash){
            return true;
        }
        return false;
    }

    /**
     * @param $file
     * @return bool
     */
    private function checkFile($file){
        if(filesize($file)>$this->max_file_size){
            return false;
        }
        return true;
    }

    /**
     * @param $file
     * @return bool
     */
    private function checkFileIsEmpty($file){
        if(filesize($file)<=1){
            return false;
        }
        return true;
    }

    /**
     * @param $config
     */
    public function setConfig($config){
        $this->img_file_name = APP_HOME . DS . $config['img_file_name'];
        $this->img_dir = APP_HOME . DS . $config['img_path'];
        $this->def_ext = $config['def_ext'];
        $this->max_file_size = $config['max_file_size'];
        $this->log_file = $config['log_file'];
        $this->default = $config['default'];
    }

    /**
     * @param $file
     * @param $def_ext
     */
    public function saveNewImage($file, $def_ext){
        $new_name = generateUniqueName($def_ext);
        $this->updateNewFileName($new_name);
        $this->saveUploadedFile($file, $new_name);
        $type = "event";
        $message = "get new file, saved with name $new_name";
        addToLog($message, currentDatetime(), $type, $this->log_file);
    }

    /**
     * @param $file
     * @param $new_name
     */
    private function saveUploadedFile($file, $new_name){
        $file = file_get_contents($file);
        $final_name = $this->img_dir . DS . $new_name;
        file_put_contents($final_name, $file);
        chmod($final_name, 0777);
    }

    /**
     * @return bool
     */
    private function isFilesaverExists(){
        if(file_exists($this->img_file_name)){
            return true;
        }
        return false;
    }

    /**
     * @param $new_filename
     */
    public function updateNewFileName($new_filename){
        if(!$this->isFilesaverExists()){
            returnError(self::filename_is_is_not_exists);
        }
        file_put_contents($this->img_file_name, $new_filename);
        //chmod($this->img_file_name, 0777);
    }

}