<?php
if (php_sapi_name() !== "cli") {
    die("installation can be launched in cli mode only!");
}
header('Content-Type: text/html; charset=utf-8');
require_once "config.php";
require_once "functions.php";

//1. create file filename
file_put_contents($abs_filename, null);
chmod($abs_filename, 0777);
$check_filename = file_get_contents($abs_filename);
if (!file_exists($abs_filename)) {
    die("Application can't create required file 'filename', application is stopped...");
}

//2. create file pass
$new_pass = generateAppPass();
file_put_contents($pass_file, $new_pass);
chmod($pass_file, 0777);
$check_pass = file_get_contents($pass_file);
if (!file_exists($pass_file)) {
    die("Application can't create required key file 'pass', application is stopped...");
}

//3. create file status
file_put_contents($status_file, null);
chmod($status_file, 0777);
$check_status_file = file_get_contents($status_file);
if (!file_exists($status_file)) {
    die("Application can't create required file 'status', application is stopped...");
}

//4. create file app.log
file_put_contents($log_file, null);
chmod($log_file, 0777);
if (!file_exists($log_file)) {
    die("Application can't create required file 'app.log', application is stopped...");
}

//5. create images dir
$dir_images = APP_HOME . DS . $img_path;
if(!is_dir($dir_images)){
    mkdir($dir_images);
    chmod($dir_images, 0777);
}

$type = "APP CONFIG SETUP";
$message = "settings was changed";
addToLog($message, currentDatetime(), $type, $log_file);
echo "Application DocViewer was configured successfully! 
Now you need create default image file in directory APP/images/ and setup default image filename in APP/config.php (variable 'file_default') before application will be started";
exit;