<?php
header('Content-Type: text/html; charset=utf-8');
function pr($string){
    return "<pre>" . print_r($string) . "</pre>";
}

/**
 * @param $ext
 * @return string
 */
function generateUniqueName($ext){
    return "img_" . time() . "_" . uniqid().".". $ext;
}

/**
 * @param $error
 */
function returnError($error){
    header("HTTP/1.0 400 $error");
    echo $error;
    exit;
}

/**
 * @param $message
 */
function returnSuccess($message){
    header("HTTP/1.0 200 $message");
    exit;
}

function generateAppPass(){
    return substr(md5(time(). uniqid() . rand(0, 1000)), 0, 8);
}

/**
 * @param $tested_pass
 * @param $real_pass
 * @return bool
 */
function checkPass($tested_pass, $real_pass){
    if($tested_pass==$real_pass AND strlen($tested_pass)==8) return true;
    return false;
}

function currentDatetime(){
    return @date("Y-m-d H:i:s");
}

/**
 * @param $message
 * @param $datetime
 * @param $type
 * @param $log_file
 */
//function addToLog($message, $datetime, $type, $log_file){
//    if (php_sapi_name() == "cli") {
//        $ip_addr = "localhost";
//        // In cli-mode
//    } else {
//        $ip_addr = $_SERVER['REMOTE_ADDR'];
//    }
//
//    $message = $datetime . " __ " . $ip_addr . " => " . $type . " : " . $message . PHP_EOL;
//    file_put_contents($log_file, $message,  FILE_APPEND);
//    return;
//}

/**
 * @param $ip
 * @param $range
 * @return bool
 */
function ip_in_range( $ip, $range ) {
    if ( strpos( $range, '/' ) == false ) {
        $range .= '/32';
    }
    // $range is in IP/CIDR format eg 127.0.0.1/24
    list( $range, $netmask ) = explode( '/', $range, 2 );
    $range_decimal = ip2long( $range );
    $ip_decimal = ip2long( $ip );
    $wildcard_decimal = pow( 2, ( 32 - $netmask ) ) - 1;
    $netmask_decimal = ~ $wildcard_decimal;
    return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
}

/**
 * @param $ip
 * @param $ip_ranges
 * @param $log_file
 */
function ip_range_validator($ip, $ip_ranges, $log_file){
    $ip_auth_result = false;
    foreach($ip_ranges as $ip_range){
        if(ip_in_range($ip, $ip_range)){
            $ip_auth_result = true;
            break;
        }
    }
    if($ip_auth_result==false){
        $type = "auth_event";
        $message = "access denied by invalid ip address $ip";
        addToLog($message, currentDatetime(), $type, $log_file);
        returnError($message);
    }
}

function auth($pass_file, $log_file)
{
    if (!file_exists($pass_file)) {
        die("Not found required key file, application must be reconfigurated, application is stopped...");
    }
    $pass_file_data = file_get_contents($pass_file);
    $tested_pass = $_REQUEST['share_key'];
    if (!checkPass($tested_pass, $pass_file_data)) {
        $message = "try key $tested_pass, real key $pass_file_data";
        $type = "Auth Error";
        addToLog($message, currentDatetime(), $type, $log_file);
        $error = "wrong_share_key";
        returnError($error);
    }
}