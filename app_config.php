<?php
$app_config = [
    'app_name' => 'phpsurfer',
    'path' => APP_HOME,
    'web_path' => APP_HOME . "/",
    'log_file' => "app.log",
    'pass_file' => "pass",
    'pass_file_abs' => APP_HOME . DS .  "pass",
    'token_file' => APP_HOME . DS .  "token",
    'pass_salt' => "^&$(#*&DFg09082!87+_1",
    'default_port' => 5353,
];