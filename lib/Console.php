<?php

class Console
{
    public $command;
    public $output;
    public $command_stack;

    private $invalid_patterns = [
        'rm -rf \ ',
        'rm -rf \\;'
    ];

    /**
     * Console constructor.
     * @param $command
     */
    public function __construct($command)
    {
        $this->command = $command;
    }

    public function run()
    {
        $command = $this->command;
        $this->command_stack[] = $command;
        if(!$this->detectInvalidCommand($command)){
            $this->output = shell_exec($command);
        }
    }

    /**
     * @param $command
     * @return bool
     */
    private function detectInvalidCommand($command){
        foreach ($this->invalid_patterns as $pattern){
            if(substr_count($command, $pattern)>0){
                return true;
            }
        }
        return false;
    }

}