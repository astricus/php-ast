<?php

class Loger
{
    public $log_file;

    /**
     * Log constructor.
     */
    public static function logFile()
    {
        return APP_HOME . DS . LOG_FILE;
    }

    /**
     * @param $message
     * @param $datetime
     * @param $type
     */
    public static function addToLog($message, $datetime, $type){
        $log_file = self::logFile();
        if (php_sapi_name() == "cli") {
            $ip_addr = "localhost";
            // In cli-mode
        } else {
            $ip_addr = $_SERVER['REMOTE_ADDR'];
        }
        file_put_contents($log_file, self::logFormat($datetime, $ip_addr, $type, $message),  FILE_APPEND);
        return;
    }

    /**
     * @param $datetime
     * @param $ip_addr
     * @param $type
     * @param $message
     * @return string
     */
    private static function logFormat($datetime, $ip_addr, $type, $message){
        return $datetime . " __ " . $ip_addr . " => " . $type . " : " . $message . PHP_EOL;
    }

}