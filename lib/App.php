<?php

class App
{
    public static $config;
    private static $config_not_found = "Config element not found!";

    public static function config()
    {
        return self::$config;
    }

    public static function getConfig($elem)
    {
        if (key_exists($elem, self::$config)) {
            return self::$config[$elem];
        }
        return self::$config_not_found;
    }

    /**
     * App constructor.
     * @param $config
     */
    public function __construct($config)
    {
        self::$config = $config;;
    }

    public function getAppUri()
    {
        $server_env = $_SERVER['REQUEST_URI'];
        $request_uri = str_replace("/", "", $server_env);
        if (substr_count($request_uri, "?") > 0) {
            $R_uri = explode("?", $request_uri);
            $request_uri = $R_uri[0];
        }
        return $request_uri;
    }

    /**
     * @param $data
     * @param $status
     */
    public static function returnJson($data, $status)
    {
        $data = ["status" => $status, "data" => $data];
        echo json_encode($data);
    }

    /**
     * @param $error
     */
    static function returnError($error)
    {
        header("HTTP/1.0 400 $error");
        echo $error;
        exit;
    }

    /**
     * @param $message
     */
    static function returnSuccess($message)
    {
        header("HTTP/1.0 200 $message");
        exit;
    }

    /**
     * @param $string
     * @return string
     */
    public static function pr($string)
    {
        return "<pre>" . print_r($string) . "</pre>";
    }

}
