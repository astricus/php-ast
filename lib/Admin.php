<?php

class Admin
{
    public $user;
    public $authorize_page = "/web/authorize.php";

    /**
     * Admin constructor.
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * @param $token_file
     * @return bool
     */
    private function checkUserIsAuth($token_file){
        if(isset($_SESSION['is_auth']) AND file_exists($token_file) && file_get_contents($token_file) == $_SESSION['token']){
            return true;
        } return false;
    }

    /**
     * @param $token_file
     */
    public function checkAuth($token_file){
        session_start();
        if(!$this->checkUserIsAuth($token_file) && $_SERVER['REQUEST_URI']!=$this->authorize_page){
            header("Location: $this->authorize_page");
            exit;
        }
        if(isset($_REQUEST['action']) && $_REQUEST['action'] == "logout"){
            $this->logout($token_file);
            exit;
        }
    }

    public function auth($pass_file)
    {
        if (!file_exists($pass_file)) {
            die("Not found required key file, application must be reconfigured, application is stopped...");
        }
        $pass_file_data = file_get_contents($pass_file);
        $tested_pass = $_REQUEST['share_key'];
        if (!checkPass($tested_pass, $pass_file_data)) {
            $message = "try key $tested_pass, real key $pass_file_data";
            $type = "Auth Error";
            Loger::addToLog($message, currentDatetime(), $type);
            $error = "wrong_share_key";
            App::returnError($error);
        }
    }

    /**
     * @param $pass_salt
     * @param $pass
     * @return string
     */
    public function stringHash($pass_salt, $pass)
    {
        return md5($pass . md5($pass_salt . $pass));
    }

    /**
     * @param $pass_file_abs
     * @param $pass_salt
     * @return bool
     */
    public function authorizeUser($pass_file_abs, $pass_salt, $token_file)
    {
        $test_pass = $_REQUEST['password'];
        $real_pass = file_get_contents($pass_file_abs);
        if ($this->stringHash($pass_salt, $test_pass) === $real_pass) {
            $this->forceAuthorize($real_pass, $token_file);
            return true;
        }
        return false;
    }

    /**
     * @param $real_pass
     * @param $token_file
     * @return false|string
     */
    public function setToken($real_pass, $token_file)
    {
        $token = substr(md5(time() . uniqid() . md5($real_pass)), 0, 8);
        $this->saveToken($token, $token_file);
        return $token;
    }

    /**
     * @param $token
     * @param $tokenFile
     */
    public function saveToken($token, $tokenFile)
    {
        file_put_contents($tokenFile, $token);
    }

    /**
     * @param $tokenFile
     * @return false|string
     */
    public function getToken($tokenFile)
    {
        return file_get_contents($tokenFile);
    }

    /**
     * @param $real_pass
     * @param $token_file
     * @return false|string
     */
    public function forceAuthorize($real_pass, $token_file)
    {
        session_start();
        $_SESSION['is_auth'] = true;
        $_SESSION['token'] = $this->setToken($real_pass, $token_file);
        return $_SESSION['token'];
    }

    /**
     * @param $token_file
     */
    public function logout($token_file)
    {
        session_start();
        unset($_SESSION['is_auth']);
        unset($_SESSION['token']);
        if (file_exists($token_file)) {
            unlink($token_file);
        }
        header("Location: /");
    }

    /**
     * @param $ext
     * @return string
     */
    public function generateUniqueName($ext)
    {
        return "img_" . time() . "_" . uniqid() . "." . $ext;
    }


    public function generateAppPass()
    {
        return substr(md5(time() . uniqid() . rand(0, 1000)), 0, 8);
    }

    /**
     * @param $tested_pass
     * @param $real_pass
     * @return bool
     */
    public function checkPass($tested_pass, $real_pass)
    {
        if ($tested_pass == $real_pass and strlen($tested_pass) == 8) return true;
        return false;
    }

    public function AuthorizeInit(){
        $result = "";
        if(isset($_REQUEST['action']) AND  $_REQUEST['action'] == "auth"){
            $pass_file_abs = App::getConfig("pass_file_abs");
            $pass_salt = App::getConfig("pass_salt");
            $token_file = App::getConfig("token_file");
            if($this->authorizeUser($pass_file_abs, $pass_salt, $token_file)){
                $token = $this->getToken($token_file);
                header("Location: /web/");
                exit;
            } else {
                $result = "error! wrong password!";
            }
        }
        return $result;
    }


}