<?php
class FileMan
{
    public $file;
    public $path;

    public function getDirList($dir)
    {
        $dir_arr = [];
        $dir_handle = opendir($dir);
        while (false !== ($entry = readdir($dir_handle))) {
            if ($entry == ".") continue;
            $elem = [];
            $elem['name'] = $entry;
            $elem['type'] = is_file($dir . DS . $entry) ? "file" : "dir";
            if (is_file($dir . DS . $entry)) {
                $elem['size'] = filesize($dir . DS . $entry);
            } else {
                $elem['size'] = null;
            }

            $dir_arr[] = $elem;
        }
        return $dir_arr;
    }

    /**
     * @param $file
     * @return bool
     */
    private function isFileExistsAndReadable($file){
        if(file_exists($file) AND is_readable($file)){
            return true;
        }
        return false;
    }

    /**
     * @param $file
     * @return false|string
     */
    private function fileContent($file){
        return file_get_contents($file);
    }

    /**
     * @param $file
     * @return bool|false|string
     */
    public function getFileContent($file)
    {
        if($this->isFileExistsAndReadable($file) AND !is_dir($file)){
            return $file_content = $this->fileContent($file);
        }
        return false;

    }

    /**
     * @param $file
     * @return bool
     */
    public function deleteFile($file)
    {
        if($this->isFileExistsAndReadable($file)){
            echo "DELETE";
            return unlink($file);
        }
    }

}