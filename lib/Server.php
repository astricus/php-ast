<?php

class Server
{
    public $server_name;
    public $os;
    public $ip;

    public function __construct()
    {
        $this->os = PHP_OS;
        $this->ip = $_SERVER['REMOTE_ADDR'];
    }

    public function serverDateTime(){
        return date("Y-m-d H:i:s") ?? time();
    }

    public function server_memory_usage()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2] / $mem[1] * 100;
        return sprintf("%1.2f", $memory_usage);
    }

    public function server_cpu_usage()
    {
        $load = sys_getloadavg();
        return sprintf("%1.2f",$load[0]);
    }

    public function disk_free_space(){
        $df = disk_free_space("/");
        return $this->format_size($df);
    }

    public function server_info(){
        return php_uname();
    }

    /**
     * @param $size
     * @return string
     */
    private function format_size($size)
    {
        $mod = 1024;

        $units = explode(' ', 'B KB MB GB TB PB');
        for ($i = 0; $size > $mod; $i++) {
            $size /= $mod;
        }
        return round($size, 2) . ' ' . $units[$i];
    }

    /**
     * @param $ip
     * @param $range
     * @return bool
     */
    function ip_in_range( $ip, $range ) {
        if ( strpos( $range, '/' ) == false ) {
            $range .= '/32';
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list( $range, $netmask ) = explode( '/', $range, 2 );
        $range_decimal = ip2long( $range );
        $ip_decimal = ip2long( $ip );
        $wildcard_decimal = pow( 2, ( 32 - $netmask ) ) - 1;
        $netmask_decimal = ~ $wildcard_decimal;
        return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
    }

    /**
     * @param $ip
     * @param $ip_ranges
     * @param $log_file
     */
    function ip_range_validator($ip, $ip_ranges, $log_file){
        $ip_auth_result = false;
        foreach($ip_ranges as $ip_range){
            if(ip_in_range($ip, $ip_range)){
                $ip_auth_result = true;
                break;
            }
        }
        if($ip_auth_result==false){
            $type = "auth_event";
            $message = "access denied by invalid ip address $ip";
            addToLog($message, currentDatetime(), $type, $log_file);
            returnError($message);
        }
    }


}